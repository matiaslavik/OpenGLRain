#pragma once
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtx/transform.hpp"
#include <vector>
#include <string>

struct ShaderProgram
{
    GLuint programID;
};

struct VertexBuffer
{
    GLuint vbo;
    GLuint vao;
};

struct IndexBuffer
{
    GLuint ibo;
};

class GLUtils
{
public:
    static void printErrors();
    static ShaderProgram createShaderProgram(std::string vsPath, std::string gsPath, std::string fsPath);
    static VertexBuffer ceateVertexBuffer(std::vector<glm::vec3>& vertices);
    static IndexBuffer ceateIndexBuffer(std::vector<unsigned int>& indices);
    static std::string readFileToString(const std::string filePath);
};
