#include <iostream>
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <chrono>
#include "particle_system.h"
#include "scenery.h"

GLFWwindow* window = nullptr;
Scenery* mScenery = nullptr;
ParticleSystem* particleSystem = nullptr;

const int WINDOW_WIDTH = 1024;
const int WINDOW_HEIGHT = 768;

void render(float deltaTime)
{
    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    glClearColor(0.3f, 0.3f, 0.6f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 proj = glm::perspective(glm::radians(120.0f), 800.0f / 600.0f, 0.01f, 100.0f); 
    glm::mat4 view = glm::lookAt(glm::vec3(0.0f, 0.5f, -1.5f), glm::vec3(0.0f, 0.5f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    
    // Render scenery
    mScenery->render(deltaTime, view, proj);
    
    // Render particles
    particleSystem->render(deltaTime, view, proj);

    glfwSwapBuffers(window);

    glfwPollEvents();

    GLUtils::printErrors();
}

int main()
{
    // Init GLFW (need to do this first)
    glfwInit();

    // Use OpenGL 4.3 Core
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create window and context
    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "OpenGL Rain Test", NULL, NULL);
    glfwShowWindow(window);
    glfwMakeContextCurrent(window);

    // Initialise GLEW (needs to happen after we have a context)
    if(glewInit() != GLEW_OK)
        throw std::runtime_error("Failed to initialise GLEW");


    // Create the scenery (floor plane)
    mScenery = new Scenery();
    mScenery->initialise();

    // Create particle system
    particleSystem = new ParticleSystem();
    particleSystem->initialise(20000, glm::vec2(0.007f, 0.055f), glm::vec4(0.8f, 0.8f, 0.9f, 0.9f));

    auto lastTime = std::chrono::high_resolution_clock::now();

    while(!glfwWindowShouldClose(window))
    {
        // Calculate delta time
        auto currentTime = std::chrono::high_resolution_clock::now();
        float deltaTime = std::chrono::duration<float>(currentTime - lastTime).count();
        
        // Render everything
        render(deltaTime);

        lastTime = currentTime;
    }

    glfwDestroyWindow(window);

    return 0;
}
