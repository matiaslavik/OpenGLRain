#include "gl_utils.h"
#include <exception>
#include <fstream>
#include <iostream>
#include "GLFW/glfw3native.h"

void GLUtils::printErrors()
{
    GLenum errCode;
    while((errCode = glGetError()) != GL_NO_ERROR)
    {
        std::string errString = (char*)glewGetErrorString(errCode);
        std::cout << errString << std::endl;
    }
}

ShaderProgram GLUtils::createShaderProgram(std::string vsPath, std::string gsPath, std::string fsPath)
{
    GLuint vertexShader = 0;
    GLuint geometryShader = 0;
    GLuint fragmentShader = 0;

    // Vertex shader
    if(vsPath != "")
    {
        std::ifstream vsStream;
        vsStream.open(vsPath);
        assert(vsStream.is_open());

        std::string vsContent((std::istreambuf_iterator<char>(vsStream)), std::istreambuf_iterator<char>());
        const char* vsContentCStr = vsContent.c_str();
        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, 1, &vsContentCStr, nullptr);
        glCompileShader(vertexShader);

        vsStream.close();
    }

    // Geometry shader
    if(gsPath != "")
    {
        std::ifstream gsStream;
        gsStream.open(gsPath);
        assert(gsStream.is_open());

        std::string gsContent((std::istreambuf_iterator<char>(gsStream)), std::istreambuf_iterator<char>());
        const char* gsContentCStr = gsContent.c_str();
        geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(geometryShader, 1, &gsContentCStr, nullptr);
        glCompileShader(geometryShader);

        gsStream.close();
    }

    // Fragment shader
    if(fsPath != "")
    {
        std::ifstream fsStream;
        fsStream.open(fsPath);
        assert(fsStream.is_open());

        std::string fsContent((std::istreambuf_iterator<char>(fsStream)), std::istreambuf_iterator<char>());
        const char* fsContentCStr = fsContent.c_str();
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, 1, &fsContentCStr, nullptr);
        glCompileShader(fragmentShader);

        fsStream.close();
    }

    // Shader program
    GLuint shaderProgram = glCreateProgram();
    if(vsPath != "")
        glAttachShader(shaderProgram, vertexShader);
    if(gsPath != "")
        glAttachShader(shaderProgram, geometryShader);
    if(fsPath != "")
        glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    GLint success = 0;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if(!success)
    {
        GLchar errorLog[512];
        glGetProgramInfoLog(shaderProgram, 512, NULL, errorLog);
        throw std::runtime_error(errorLog);
    }

    printErrors();

    ShaderProgram program;
    program.programID = shaderProgram;
    return program;
}

VertexBuffer GLUtils::ceateVertexBuffer(std::vector<glm::vec3>& vertices)
{
    // Create vertex buffer object
    GLuint vbo = 0;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), vertices.data(), GL_STATIC_DRAW);

    // Bind vertex attributes
    GLuint vao; // TODO: Must return this, for later cleanup
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);

    printErrors();

    VertexBuffer vertexBuffer;
    vertexBuffer.vbo = vbo;
    vertexBuffer.vao = vao;
    return vertexBuffer;
}

IndexBuffer GLUtils::ceateIndexBuffer(std::vector<unsigned int>& indices)
{
    GLuint ibo = 0;
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);

    printErrors();

    IndexBuffer indexBuffer;
    indexBuffer.ibo = ibo;
    return indexBuffer;
}

std::string GLUtils::readFileToString(const std::string filePath)
{
    std::ifstream csStream;
    csStream.open(filePath);
    assert(csStream.is_open());
    return std::string((std::istreambuf_iterator<char>(csStream)), std::istreambuf_iterator<char>());
}
