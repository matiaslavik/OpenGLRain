#pragma once
#include "gl_utils.h"
#include "glm/glm.hpp"

// Scenery: Renders a floor plane.
class Scenery
{
private:
    ShaderProgram mShaderProgram;
    VertexBuffer mVertexBuffer;
    IndexBuffer mIndexBuffer;

    // Uniform locations
    GLuint mModelMatLoc = 0;
    GLuint mViewMatLoc = 0;
    GLuint mProjMatLoc = 0;
    GLuint mColourLoc = 0;

public:
    void initialise();
    void render(float deltaTime, glm::mat4 view, glm::mat4 proj);
};
