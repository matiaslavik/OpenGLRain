#include "particle_system.h"
#include <fstream>
#include <cassert>
#include <cstring>

void ParticleSystem::initialise(int numParticles, glm::vec2 particleSize, glm::vec4 particleColour)
{
    mNumParticles = numParticles;
    mParticleSize = particleSize;
    mParticleColour = particleColour;

    /* ----- STEP 1: Create and initialise particle shader program and buffers. ----- */
    // This is for rendering the particles
    // We use 3 shaders:
    //   Vertex shader: For each point.
    //   Geometry shader: Converts point to a rectangle.
    //   Fragment shader: Adds colour to the particle.

    // Create partile list
    std::vector<glm::vec4> particleVerts;
    for(int i = 0; i < mNumParticles; i++)
        particleVerts.push_back(glm::vec4(((float)rand() / (RAND_MAX / 2)) - 1.0f, ((float)rand() / (RAND_MAX / 2)) - 1.0f, ((float)rand() / (RAND_MAX / 2)) - 1.0f, 0.0f));

    // Create vertex buffer
    glGenBuffers(1, &mPositionBuffer);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, mPositionBuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, particleVerts.size() * sizeof(glm::vec4), NULL, GL_STATIC_DRAW);
    GLint bufMask = GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT;
    void* buffer = glMapBufferRange( GL_SHADER_STORAGE_BUFFER, 0, particleVerts.size() * sizeof(glm::vec4), bufMask );
    memcpy(buffer, particleVerts.data(), particleVerts.size() * sizeof(glm::vec4));
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    GLUtils::printErrors();

    // Create vertex arry object
    glGenVertexArrays(1, &ptclVertBuff.vao);
    glBindVertexArray(ptclVertBuff.vao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, mPositionBuffer);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(glm::vec4), 0);
    GLUtils::printErrors();

    // Create particle shader program
    ptclShdrPrg = GLUtils::createShaderProgram("../../shaders/particle.vs", "../../shaders/particle.gs", "../../shaders/particle.fs");

    // Get shader uniform locations (particle shader program)
    mModelMatLoc = glGetUniformLocation(ptclShdrPrg.programID, "modelMat");
    mViewMatLoc = glGetUniformLocation(ptclShdrPrg.programID, "viewMat");
    mProjMatLoc = glGetUniformLocation(ptclShdrPrg.programID, "projMat");
    mParticleSizeLoc = glGetUniformLocation(ptclShdrPrg.programID, "size");
    mParticleColLoc = glGetUniformLocation(ptclShdrPrg.programID, "colour");


    /* ----- STEP 2: Create and initialise compute shader program ----- */
    // The compute shader is responsible for updating the position of each particle.

    // Create compute shader program
    std::string csContent = GLUtils::readFileToString("../../shaders/particle.cs");
    const char* csContentCStr = csContent.c_str();
    mCSProg = glCreateProgram();
    GLuint cs = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(cs, 1, &csContentCStr, NULL);
    glCompileShader(cs);
    glAttachShader(mCSProg, cs);

    glLinkProgram(mCSProg);
    GLint success = 0;
    glGetProgramiv(mCSProg, GL_LINK_STATUS, &success);
    if(!success)
    {
        GLchar errorLog[512];
        glGetProgramInfoLog(mCSProg, 512, NULL, errorLog);
        throw std::runtime_error(errorLog);
    } 

    // Get shader uniform locations (compute shader program)
    mCSTimeLoc = glGetUniformLocation(mCSProg, "deltaTime");
    mCSSpeedLoc = glGetUniformLocation(mCSProg, "speed");
    
    GLUtils::printErrors();
}

void ParticleSystem::render(float deltaTime, glm::mat4 view, glm::mat4 proj)
{
    /* STEP 1: Execute compute shader */

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1,  mPositionBuffer);
    glUseProgram(mCSProg);

    glUniform1f(mCSTimeLoc, deltaTime);
    glUniform1f(mCSSpeedLoc, 0.5f);

    glDispatchCompute(mNumParticles  / WORK_GROUP_SIZE, 1,  1);

    // The compute shader writes to the same buffer as the particle rendering reads, so we need to make sure this happens in order.
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);


    /* STEP 2: Render the particles. */

    glm::mat4 particleMat = glm::scale(glm::vec3(20.0f, 20.0f, 20.0f));

    // Update uniforms
    glUseProgram(ptclShdrPrg.programID);
    glUniformMatrix4fv(mModelMatLoc, 1, GL_FALSE, &particleMat[0][0]);
    glUniformMatrix4fv(mViewMatLoc, 1, GL_FALSE, &view[0][0]);
    glUniformMatrix4fv(mProjMatLoc, 1, GL_FALSE, &proj[0][0]);
    glUniform2fv(mParticleSizeLoc, 1, (float*)&mParticleSize[0]);
    glUniform4fv(mParticleColLoc, 1, (float*)&mParticleColour[0]);
    
    // Enable blending (for transparency)
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Enable depth test and disable depth write
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);

    // Bind vertex buffer and vertex array object
    glBindBuffer( GL_ARRAY_BUFFER, mPositionBuffer);
    glBindVertexArray(ptclVertBuff.vao);
    // Render as points (geometry shader converts each point to a quad)
    glDrawArrays(GL_POINTS, 0, mNumParticles);
    
    GLUtils::printErrors();
}
