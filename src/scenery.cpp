#include "scenery.h"
#include <vector>

void Scenery::initialise()
{
    mShaderProgram = GLUtils::createShaderProgram("../../shaders/default_colour.vs", "", "../../shaders/default_colour.fs");
    
    std::vector<glm::vec3> vertices = { glm::vec3(-0.5f, 0.5f, 0.0f), glm::vec3(0.5f, 0.5f, 0.0f), glm::vec3(0.5f, -0.5f, 0.0f), glm::vec3(-0.5f, -0.5f, 0.0f) };
    std::vector<unsigned int> indices = {0, 1, 3, 3, 1, 2};

    mVertexBuffer = GLUtils::ceateVertexBuffer(vertices);
    mIndexBuffer = GLUtils::ceateIndexBuffer(indices);

    // Get shader uniform locations
    mModelMatLoc = glGetUniformLocation(mShaderProgram.programID, "modelMat");
    mViewMatLoc = glGetUniformLocation(mShaderProgram.programID, "viewMat");
    mProjMatLoc = glGetUniformLocation(mShaderProgram.programID, "projMat");
    mColourLoc = glGetUniformLocation(mShaderProgram.programID, "colour");
}

void Scenery::render(float deltaTime, glm::mat4 view, glm::mat4 proj)
{
    glUseProgram(mShaderProgram.programID);

    glm::mat4 modelMat = glm::rotate(1.57f, glm::vec3(1.0f, 0.0f, 0.0f)) * glm::scale(glm::vec3(10.0f, 10.0f, 10.0f));
    glm::vec4 colour = glm::vec4(0.0f, 0.4f, 0.0f, 1.0f);

    // Update uniforms.
    glUniformMatrix4fv(mModelMatLoc, 1, GL_FALSE, &modelMat[0][0]);
    glUniformMatrix4fv(mViewMatLoc, 1, GL_FALSE, &view[0][0]);
    glUniformMatrix4fv(mProjMatLoc, 1, GL_FALSE, &proj[0][0]);
    glUniform4fv(mColourLoc, 1, (float*)&colour[0]);
    
    glBindVertexArray(mVertexBuffer.vao);

    // Enable depth test and depth write
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer.ibo);
    glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(6), GL_UNSIGNED_INT, 0);
}
