#pragma once

#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtx/transform.hpp"
#include "gl_utils.h"

class ParticleSystem
{
private:
    ShaderProgram ptclShdrPrg;
    VertexBuffer ptclVertBuff;
    IndexBuffer ptclIndBuff;
    GLuint mCSProg = 0;
    GLuint mPositionBuffer = 0;

    // Uniform locations (computer shader)
    GLuint mCSTimeLoc = 0;
    GLuint mCSSpeedLoc = 0;

    // Uniform locations (particle shaders)
    GLuint mModelMatLoc = 0;
    GLuint mViewMatLoc = 0;
    GLuint mProjMatLoc = 0;
    GLuint mParticleSizeLoc = 0;
    GLuint mParticleColLoc = 0;

    int mNumParticles = 0;
    glm::vec2 mParticleSize;
    glm::vec4 mParticleColour;

    const int WORK_GROUP_SIZE = 128;

public:
    void initialise(int numParticles, glm::vec2 particleSize, glm::vec4 particleColour);
    void render(float deltaTime, glm::mat4 view, glm::mat4 proj);
};
