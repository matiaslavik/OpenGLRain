# OpenGL particle system example (with compute shader)

This is an example of rendering particles in OpenGL.

**Features:**
- The particles are updated by a compute shader.
- The particles are rendered as points, and a geometry shader converts the points to rectangles.

<img src="screenshots/screenshot-1.png" width="600px">

## Building
**Prerequisites:**
- CMake
- Conan package manager

**Steps:**
1. Install Conan Package Manager: https://conan.io/
2. Create a build folder and run "conan install" inside it.
3. Build the project using CMake

**Example (building the project on Linux):**
1. mkdir build
2. cd build
3. conan install ..
4. cmake ..
5. make

## Contributing
This is an example project, made to be easy to understand. It will not be updated to improve the visuals or performance, or adding new features.
However, if you spot any mistakes feel free to create an issue or a pull request! :)
