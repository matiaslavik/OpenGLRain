#version 430 core

uniform vec4 colour;

void main()
{
    gl_FragColor = colour;
}
