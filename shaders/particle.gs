// Particle geometry shader
// Turns points into rectangles
#version 430 core
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform mat4 projMat;
uniform vec2 size;

out vec2 uv;

void main()
{
    vec4 pos = gl_in[0].gl_Position;
    float pointWidth = size.x;
    float pointHeight = size.y;

    gl_Position = projMat * (pos + vec4(-pointWidth, -pointHeight, 0.0f, 0.0f));
    uv = vec2(0.0f, 0.0f);
    EmitVertex();
    gl_Position = projMat * (pos + vec4(pointWidth, -pointHeight, 0.0f, 0.0f));
    uv = vec2(1.0f, 0.0f);
    EmitVertex();
    gl_Position = projMat * (pos + vec4(-pointWidth, pointHeight, 0.0f, 0.0f));
    uv = vec2(0.0f, 1.0f);
    EmitVertex();
    gl_Position = projMat * (pos + vec4(pointWidth, pointHeight, 0.0f, 0.0f));
    uv = vec2(1.0f, 1.0f);
    EmitVertex();
    
    EndPrimitive();
}
