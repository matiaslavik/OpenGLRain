#version 430 core

uniform vec4 colour;

in vec2 uv;

out vec4 fragColour;

void main()
{
    vec4 col = colour;
    // Make particle be opaque at the centre, and transparent towads the edges
    col.a = smoothstep(col.a, 0.0f, abs(uv.x - 0.5f) * 2.0f) * smoothstep(col.a, 0.0f, abs(uv.y - 0.5f) * 2.0f);
    
    gl_FragColor = col;
}
