// Compute shader.
// Moves the position of the particles.
#version 430 core

layout( local_size_x = 128, local_size_y = 1, local_size_z = 1) in;

// Particle position buffer (we read from and write to this)
layout (std140, binding = 1) buffer positionBuffer
{
    vec4 positions[];
};

// Time since last update
uniform float deltaTime;
// Particle speed
uniform float speed;

void main()
{
    uint gid = gl_GlobalInvocationID.x;
    vec3 pos = positions[gid].xyz;

    // Update particle position
    pos.y = pos.y - speed * deltaTime;;

    // If particle fell below the
    if(pos.y < -0.5f)
        pos.y += 1.0f;

    // Wrap y position to range (-0.5, 0.5), to make particles that fell below the bottom (-0.5) re-spawn at the top.
    pos.y = mod(pos.y + 0.5f, 1.0f) - 0.5f;

    positions[gid].xyz = pos;
}
