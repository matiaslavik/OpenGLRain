#version 430 core

layout (location=0) in vec4 inPos;

uniform mat4 modelMat;
uniform mat4 viewMat;

void main()
{
    // Multiply by model matrix and camera view matrix (projection is done in the geometry shader)
    gl_Position = viewMat * modelMat * vec4(inPos.xyz, 1.0f);
}
